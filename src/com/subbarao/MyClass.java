package com.subbarao;

import java.util.Collections;
import java.util.List;

public class MyClass implements  MyInterface{


    @Override
    public Integer getMaxNum(List<Integer> intList) {
        return Collections.min(intList);
    }

    public List<Student> sortStudents(List<Student> studentList){
        Collections.sort(studentList);
        System.out.println("its myclass sortStudents");
        return studentList;
    }
}
