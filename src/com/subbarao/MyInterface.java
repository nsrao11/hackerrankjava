package com.subbarao;

import java.util.Collections;
import java.util.List;

public interface MyInterface {

    default  public List<Student> sortStudents(List<Student> studentList){
        Collections.sort(studentList);
        return studentList;
    }
    public static void greet(String  name){
        System.out.println("Welcome "+ name);
    }

    public abstract Integer getMaxNum(List<Integer> intList);
}
