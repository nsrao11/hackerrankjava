package com.subbarao;

import java.util.ArrayList;
import java.util.List;

public class Test {

    public static void main(String[] args) {

        MyInterface myInterface=new MyClass();
        List<Student> studentList=new ArrayList<>();
        studentList.add(new Student("SubbaRao",40));
        studentList.add(new Student("Simha",38));
        studentList.add(new Student("Suresh",36));
        List<Student> students = myInterface.sortStudents(studentList);
        students.forEach(System.out::println);

        MyInterface.greet("Subbarao");
        List<Integer> intList=new ArrayList<>();
        intList.add(10);
        intList.add(100);
        intList.add(44);
        intList.add(88);
        Integer maxNum = myInterface.getMaxNum(intList);
        System.out.println(maxNum);

    }


}
