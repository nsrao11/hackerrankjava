package com.subbarao;

import java.util.Collections;
import java.util.List;

public class MyInterface2 {

    public List<Student> sortStudents(List<Student> studentList){
        Collections.sort(studentList);
        return studentList;
    }

}

