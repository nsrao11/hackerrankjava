package com.subbarao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@AllArgsConstructor
@Setter
@Getter
public class Student implements Comparable<Student>{

    private String name;
    private Integer age;


    @Override
    public int compareTo(Student o) {
        return this.getName().compareTo(o.getName());
    }
}
