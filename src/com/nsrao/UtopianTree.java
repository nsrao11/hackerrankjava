package com.nsrao;

import java.util.Scanner;

public class UtopianTree {


    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int T=sc.nextInt();
        for(int i=0;i< T;i++){
            int cycle=sc.nextInt();
            System.out.println(utopianTree(cycle));
        }
    }

    private static int utopianTree(int cycle) {
        return (1<<((cycle>>1) +1)) - 1<<cycle % 2;
    }
}
