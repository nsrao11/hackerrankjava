package com.nsrao;

import java.util.Scanner;

public class CommonChildSolution {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String s1=sc.next();
        String s2=sc.next();
        int result=commonChild(s1,s2);
        System.out.println(result);
        sc.close();
    }

    private static int commonChild(String s1, String s2) {
        return LongestCommonSubString.LCSM4(s1.toCharArray(),s2.toCharArray(),s1.length(),s2.length());
    }
}
