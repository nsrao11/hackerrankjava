package com.nsrao;

import java.util.Scanner;

public class RepeatedString {

    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        String str=scanner.next();
        long n=scanner.nextLong();
        long aCount=repeatedString(str,n);
        System.out.println("Number of occurences of a in String"+ aCount);
        scanner.close();

    }

     static long repeatedString(String str, long n) {
        int strLength=str.length();
        long q=0,r=0;
        q=n / strLength;
        r=n % strLength;
        long partialStrLength= (r ==0 )?0 : r;
        long aCount=q* getLetterCount(str, str.length()) + getLetterCount(str,partialStrLength);
        return aCount;
    }

    private static long getLetterCount(String str, long length) {
        int count=0;
        for(int i=0;i<length;i++){
            if(str.charAt(i) == 'a')
                count++;
        }
      return count;
    }
}
