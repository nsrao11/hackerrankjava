package com.nsrao;

import java.util.Scanner;

public class JumpingOnClouds {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n=sc.nextInt();
        int[] c=new int[n];
        for(int i=0;i< n;i++){
            c[i]=sc.nextInt();
        }
        int count=jumpingOnClouds(c);
        System.out.println(count);
    }

    private static int jumpingOnClouds(int[] c) {
        int len=c.length;
        int count=-1;
        for(int i=0;i<len;i++,count++){
            if(i+2 < len && c[i+2] == 0){
                i=i++;
            }
        }
    return count;
    }

}
